/* De database legen om dubbele entiteiten te voorkomen.*/
DROP DATABASE onzeDB;
CREATE DATABASE onzeDB; 
USE onzeDB;

SELECT "Ik heb code verwijderd, hahahahahaa >:D";

CREATE TABLE Persoon (
id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
naam VARCHAR(50) NOT NULL UNIQUE, 
email VARCHAR(50) NOT NULL UNIQUE,
adres VARCHAR(50)  NOT NULL,
tegoed DECIMAL(4,2) DEFAULT 0
)
ENGINE=InnoDB;

CREATE TABLE Aanbieder (
id SMALLINT UNSIGNED NOT NULL PRIMARY KEY,
waardering DECIMAL(2,1) DEFAULT 0,
aantal_diensten SMALLINT UNSIGNED DEFAULT 0, 
CONSTRAINT id_persoon
	FOREIGN KEY (id)
	REFERENCES Persoon(id)
)
ENGINE=InnoDB;

CREATE TABLE Voedsel(
id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
aanbieder SMALLINT UNSIGNED NOT NULL,
naam VARCHAR(50) NOT NULL,
plaatje VARCHAR(70), /* TODO: Het goede type neerzetten.*/
beschrijving TEXT,
vegetarisch TINYINT(1) DEFAULT 0,
halal TINYINT(1) DEFAULT 0,
status TINYINT(1) DEFAULT 0,
CONSTRAINT id_aanbieder_van_voedsel
	FOREIGN KEY (aanbieder)
	REFERENCES Aanbieder(id)
)
ENGINE=InnoDB;

CREATE TABLE Transactie (
id MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
voedsel INT UNSIGNED NOT NULL UNIQUE, 
klant SMALLINT UNSIGNED NOT NULL,
tijdstip TIMESTAMP NOT NULL,
CONSTRAINT id_voedsel_transactie
	FOREIGN KEY (voedsel)
	REFERENCES Voedsel(id),
CONSTRAINT id_klant_transactie
	FOREIGN KEY (klant)
	REFERENCES Persoon(id)
/* Misschien nog een uniciteitseis op het paar (klant,tijdstip)? */
)
ENGINE=InnoDB;

CREATE TABLE Recensie (
id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
transactie MEDIUMINT UNSIGNED NOT NULL UNIQUE, 
recensietekst TEXT,
waardering TINYINT(5) UNSIGNED NOT NULL,
CONSTRAINT id_transactie
	FOREIGN KEY (transactie)
	REFERENCES Transactie(id)
)
ENGINE=InnoDB;

/*
TODO:
Triggers maken.
Uitzoeken of we bewust een bepaalde motor moeten kiezen.
Meerdere adressen per persoon toelaten?
met SOORTEN gerechten werken ipv met losse gerechten?
Misschien Transactie en Recensie samenvoegen?
*/


